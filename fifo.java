import java.util.LinkedList;
import java.util.Queue;

public class Fifo {
	public static void main(String[] args) {
		Queue<String> fila = new LinkedList<String>();
		
		//insere 
		for (int i=0; i<=5000;i++){
			fila.add(String.valueOf(i));
			System.out.println(" adicionou item:"+i);
			
		}
		
		//mostra sem remover
		for (String s: fila){
			System.out.println("mostrou  item: "+s);
		}
		
		
		//remove
		for (int i=0; i<=fila.size();i++){
			System.out.println(" removeu item:"+fila.remove());
		}
		System.out.println("tamanho da fila: "+fila.size());
	}
}
